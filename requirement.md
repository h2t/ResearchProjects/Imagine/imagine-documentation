![logo](./images/logo.png)

# Requirement 

This chapter describes the installation the required software to be able to start and run the KITGripper. The ArmarX-software has been developed for and tested on Ubuntu 18.04 LTS. Any derived operating systems like kubuntu or similar should also work.

This manual expects the reader to be familiar with working with Linux and the common process of compiling and running software under any Linux distribution.

## Setup a RT-Kernel

While it is not strictly necessary to use a linux kernel that supports `RT_PREEMPT` it is advised for running the Low-Level-Components in ArmarX reliable. The EtherCAT-Stack requires cycle times of less than 50 ms. If a packet takes longer to be processed (for example because other threads use the CPU too long) the entire communication fails and the robot and its software needs to be restarted.

You can skip this step, if your machine is powerful enough and any additional calculations besides the ArmarX-components are done on another machine.

Follow the [instructions](https://wiki.linuxfoundation.org/realtime/documentation/howto/applications/preemptrt_setup) with a kernel version that supports Ubuntu 18.04 LTS.



## Install TensorflowCpp 

For the ScrewLocalizer, you need TensorflowCpp. Simply clone and follow the [instructions in this repo](https://github.com/leggedrobotics/tensorflow-cpp)


