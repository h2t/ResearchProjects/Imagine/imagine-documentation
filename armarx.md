![logo](./images/logo.png)

# All in one script

<a href="https://h2t.gitlab.io/imagine-documentation/setup_armarx.sh" style="text-align:center" download>Click to Download setup_armarx.sh</a>

```shell
#!/bin/bash
sudo apt install curl -y
curl https://packages.humanoids.kit.edu/h2t-key.pub | sudo apt-key add -
echo -e "deb http://packages.humanoids.kit.edu/bionic/main bionic main\ndeb http://packages.humanoids.kit.edu/bionic/testing bionic testing" | sudo tee -a /etc/apt/sources.list.d/armarx.list

sudo apt update
sudo apt install wget h2t-armarx-meta h2t* liblmdb-dev libgsl-dev liburdfdom-dev -y

sudo update-alternatives --install /usr/bin/gcc      gcc      /usr/bin/gcc-8      100
sudo update-alternatives --install /usr/bin/g++      g++      /usr/bin/g++-8      100
sudo update-alternatives --install /usr/bin/gfortran gfortran /usr/bin/gfortran-8 100

# working directory
WS_DIR=$(pwd)
ARMARX_ROOT_DIR="${HOME}/armarx"
mkdir -p "${ARMARX_ROOT_DIR}"

# setup bullet
echo "=========================== setup bullet =================================="
BULLET_DIR="${ARMARX_ROOT_DIR}/bullet3-2.83.7/"
if [ -d "$BULLET_DIR" ] 
then
    echo "bullet exists"
else
    cd $ARMARX_ROOT_DIR
    wget https://github.com/bulletphysics/bullet3/archive/2.83.7.tar.gz
    tar xf 2.83.7.tar.gz
    rm 2.83.7.tar.gz
    mkdir -p bullet3-2.83.7/build
fi

cd $ARMARX_ROOT_DIR/bullet3-2.83.7/build
cmake .. -DCMAKE_INSTALL_PREFIX=${ARMARX_ROOT_DIR}/bullet3-2.83.7/build/install/ -DBUILD_SHARED_LIBS=on -DCMAKE_BUILD_TYPE=Release -DUSE_DOUBLE_PRECISION=on
make -j6
make install


# soem, raw_socket, simox
echo "=========================== clone soem/ImagineRT/Imagine =================================="
cd $ARMARX_ROOT_DIR
if [ ! -d "$ARMARX_ROOT_DIR/soem" ]
then
    echo "clone soem"
    git clone https://github.com/OpenEtherCATsociety/SOEM.git soem
    mkdir -p $ARMARX_ROOT_DIR/soem/build
fi

if [ ! -d "$ARMARX_ROOT_DIR/ImagineRT" ]
then
    echo "clone ImagineRT"
    cd $ARMARX_ROOT_DIR && git clone https://gitlab.com/h2t/ResearchProjects/Imagine/ImagineRT.git
    mkdir -p $ARMARX_ROOT_DIR/ImagineRT/build
fi

if [ ! -d "$ARMARX_ROOT_DIR/Imagine" ]
then
    echo "clone Imagine"
    cd $ARMARX_ROOT_DIR && git clone https://gitlab.com/h2t/ResearchProjects/Imagine/Imagine.git
    mkdir -p $ARMARX_ROOT_DIR/Imagine/build
fi

echo "=========================== setup soem =================================="
cd $ARMARX_ROOT_DIR/soem/build && cmake .. && make -j6 && sudo make install

echo "=========================== setup raw_socket =================================="
cd $ARMARX_ROOT_DIR/ImagineRT/etc/raw_socket && make clean && make && sudo make install

echo "=========================== setup simox =================================="
cd $ARMARX_ROOT_DIR
if [ ! -d "$ARMARX_ROOT_DIR/simox" ]
then
    echo "clone simox"
    git clone https://gitlab.com/Simox/simox.git
fi

cd simox/build
CXX=g++-8 cmake -DBULLET_INCLUDE_DIRS=${ARMARX_ROOT_DIR}/bullet3-2.83.7/build/install/include/bullet/ -DCMAKE_BUILD_TYPE=Release  ..
make -j6


# dmp
echo "=========================== setup DMP =================================="
cd $ARMARX_ROOT_DIR
if [ ! -d "$ARMARX_ROOT_DIR/dmp" ]
then
    echo "clone dmp"
    git clone https://gitlab.com/h2t/DynamicMovementPrimitive.git dmp
    mkdir -p dmp/build
fi

cd dmp/build && cmake .. && make -j6


# ArmarX base packages
echo "=========================== setup armarx base =================================="
cd $ARMARX_ROOT_DIR
if [ ! -d "$ARMARX_ROOT_DIR/armarx" ]
then
    echo "clone armarx"
    git clone https://gitlab.com/ArmarX/armarx.git
fi

cd armarx
git submodule update --init
git submodule foreach 'git checkout master'
 
if [ ! -d "$ARMARX_ROOT_DIR/armarx/ArmarXCore/build/" ]
then
    mkdir -p $ARMARX_ROOT_DIR/armarx/ArmarXCore/build
else
    echo "build directory existing"
fi
cd $ARMARX_ROOT_DIR/armarx/ArmarXCore/build && cmake ..
source $HOME/.bashrc
$ARMARX_ROOT_DIR/armarx/ArmarXCore/build/bin/armarx-dev build RobotSkillTemplates ArmarXSimulation
# echo "export PATH=$PATH:${ARMARX_ROOT_DIR}/armarx/ArmarXCore/build/bin:${ARMARX_ROOT_DIR}/armarx/MemoryX/build/bin" >> $HOME/.bashrc
source $HOME/.bashrc

echo "=========================== setup stable imagine packages =================================="
cd $ARMARX_ROOT_DIR/ImagineRT && git checkout demo
cd $ARMARX_ROOT_DIR/ImagineRT/build && cmake .. && make -j6
cd $ARMARX_ROOT_DIR/Imagine && git checkout demo
cd $ARMARX_ROOT_DIR/Imagine/build && cmake .. && make -j6
```

# Install dependencies

## Install SOME 

The EtherCAT-communication in ArmarX is based on [SOEM](https://openethercatsociety.github.io/doc/soem/index.html) (Simple Open EtherCAT Master). It comes with a handy tool called 'slaveinfo' which can be used from the command line to check whether the connection to the EtherCAT-slaves on the robot can be established.

```shell
git clone https://github.com/OpenEtherCATsociety/SOEM.git soem
cd soem
mkdir build
cd build
cmake ..
make
make install
```

Find you network device (wired connection) name, e.g. `enpXXX` by running 

```shell
ifconfig
```

This name will be used in the ArmarX Scenario configuration, we will get to this point later.

Check the EtherCAT connection by running

```shell
sudo slaveinfo enpXXX`
```

## Install raw_socket

raw_socket is a small program which allows ArmarX to communicate over the EtherCAT-protokol without requiring 'sudo'-rights. The software is provided in the `ImagineRT` package, clone this project first. It is important to install the program with sudo, otherwise ArmarX can not communicate with the robot.

```shell
# use ssh
git clone git@gitlab.com:h2t/ResearchProjects/Imagine/ImagineRT.git
# or use https
git clone https://gitlab.com/h2t/ResearchProjects/Imagine/ImagineRT.git

cd ImagineRT/etc/raw_socket
make
sudo make install
```

# Setup ArmarX

Follow the [Official Instructions](https://armarx.humanoids.kit.edu/ArmarXDoc-armarx-installation-ubuntu-18-04.html) to install ArmarX on you PC.

<font color="red">Please check the up-to-date instructions for compilation of `simox` package. The instructions on the official website is out-of-date and may cause compiling errors.</font>

To work with KIT Gripper, you need the following prerequisit:

order | package_name | url | status
---|---|---|---
1 | ArmarXCore | | *(already installed)*
2 | ArmarXGui  | `https://gitlab.com/ArmarX/ArmarXGui.git` |
3 | Simox | [up-to-date instructions](https://gitlab.com/Simox/simox/-/wikis/Installation-Source-Ubuntu) | *(already installed)*
4 | DMP | `https://gitlab.com/h2t/DynamicMovementPrimitive.git`| 
5 | RobotAPI | `https://gitlab.com/ArmarX/RobotAPI.git`| 
6 | MemoryX | `https://gitlab.com/ArmarX/MemoryX.git` |
7 | VisionX | `https://gitlab.com/ArmarX/VisionX.git` |

install the missing packages in order.

```
git clone <url>
cd <package_name>/build
cmake ..
make -j7
```

## Packages Related to Imagine Project

- ImagineRT

```
git clone https://gitlab.com/h2t/ResearchProjects/Imagine/ImagineRT.git
cd ImagineRT\build
cmake .. && make -j7
```

- Imagine

```
git clone https://gitlab.com/h2t/ResearchProjects/Imagine/Imagine.git
cd Imagine\build
source /PATH_TO_ROS_WORKSPACE/devel/setup.bash
cmake .. && make
# for local installation of TensorFlow cpp
cmake -DTensorFlow_DIR=~/.local/share/TensorFlow/cmake/ .. && make
```

## Update

```
armarx-dev build Imagine -p
```

- `-p` will pull all the packages that `Imagine` depends on.
- and `armarx-dev` will compile all the packages for you.

to update a single package, you need to go to the project path and 

```
git pull
cd build
cmake .. && make -j7
```

## QuickStart and Tutorials

You can find more detailed information about how to use ArmarX on [our website](https://armarx.humanoids.kit.edu/index.html)

Start Armarx
```shell
# start Ice
armarx start

# start MongoDB
# check http://armarx.humanoids.kit.edu/MemoryX-gettingstarted.html in case of errors
armarx memory start

# import environment and object models into the database, 
# ---- !!! you only need to run this command once. !!! -----
mongoimport.sh ${ArmarXDB_DIR}/data/ArmarXDB/dbexport/memdb

# start gui
armarx gui
```