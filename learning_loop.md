![logo](./images/logo.png)

# The Learning Loop
<!-- toc -->
You can always trigger the learning loop inside the main loop. This tutorial shows you how to teach the system a new disassembly action with minimum steps.


## Minimum Start of The Learning Loop

Basically you need to start the required components in ROS and in ArmarX.

## Start ROS and ADES

1. start a terminal and run ROS master
```shell
roscore
```

2. start the ADES and motion database
```
ades
```


## Start ArmarX and setup the robot

coming soon


