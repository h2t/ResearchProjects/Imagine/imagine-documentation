![logo](./images/logo.png)

# Software Structure
## A Tour to the Packages

- RobotAPI

    This package contains libraries, interfaces and components, etc that can work cross different robot. Our real time control framework and real-time controllers are implemented in this package. 
    - You can find the mostly used Task space dmp controller in this cpp file `RobotAPI/source/RobotAPI/libraries/RobotAPINJointControllers/DMPController/NJointTSDMPController.cpp`
    - for each real-time NJointController, you need to implement an ice interface to communicate with other programs that are not in the rt thread. An example of the interface for NJointTSDMPController can be found here `RobotAPI/source/RobotAPI/interface/units/RobotUnit/NJointTaskSpaceDMPController.ice`

- ImagineRT

    - The ImagineRT package contains the robot model `ImagineRT/data/ImagineRT/robotmodel` and the hardware configuration `ImagineRT/data/ImagineRT/HardwareConfig`. 
    - You can find the force torque sensor unit in `ImagineRT/source/ImagineRT/components/ImagineUnit/Devices/KITGripperFTSensor`
    - The EtherCAT library and the Imagine-related NJointControllers are located in `ImagineRT/source/ImagineRT/libraries`
    - The `NJointGripperZeroTorqueController` is Gripper specific and thus only appears in the `ImagineRT` package instead of the `RobotAPI` package. You can find it here `ImagineRT/source/ImagineRT/libraries/ImagineNJointControllers/ImagineZeroTorqueController/NJointGripperZeroTorqueController.cpp` together with its corresponding interface file `ImagineRT/source/ImagineRT/interface/ImagineNJointControllerInterface.ice`

- Imagine
    - `data`: 
        - you can find the real-time logging in `Imagine/data/Imagine/demo_logging` and 
        - the recorded trajectories (both task-space and joint-space trajectories) during kinesthetic teaching in `Imagine/data/Imagine/motions`
        - the object models for collision detection, reachability checking and visualization in `Imagine/data/Imagine/objects`
    - `ROSTaskProviderBridge` component: this component receives an affordance topic with "action names" and "action parameters" and triggers an ArmarX action (Statechart). The implementations of all the possible actions can be found here `Imagine/source/Imagine/components/ROSTaskProviderBridge/TaskMappingStrategies`
