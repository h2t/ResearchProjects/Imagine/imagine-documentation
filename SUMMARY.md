# Summary

* [Introduction](README.md)
* [Hardware Setup](hardware.md)
* [Requirement](requirement.md)
* [Setup ROS](ros.md)
* [Setup ArmarX](armarx.md)
* [Setup Python Virtual Environment](virtual_environment.md)
* [Software Structure](software_structure.md)
* [Working With KIT-Multifunctional Gripper](working_with_KIT_gripper.md)
* [Actions](actions.md)
* [Imagine System: Main Loop](main_loop.md)
* [The Learning Loop](learning_loop.md)
* [FAQs](faq.md)
