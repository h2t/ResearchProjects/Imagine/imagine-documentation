![logo](./images/logo.png)

# KIT Gripper Actions

The implemented actions are in the following groups
- `ImagineMotionGroup` which include most of the building blocks, e.g. the `DMPVelocityController`, `MoveArmToPose`, etc.
- `ImagineElementaryAction` group include some elementary actions
- `ImagineActionLibrary` group implements all the disassembly actions needed in the project demonstration
- `ImagineParameterParsing` group handles action parameters
- `ImagineDemo` group collect the statechart for giving a demo without using the ROS components.

## ADES


ADES database v2_1 final:

```
rosservice call /ades/ades2_1db/get_motion_names "ades_name: 'lever' motion_sequence: 'lever_magnet'"
```


### Conversion amoung different ADES versions (from ADESDB (version 1) to ADES2DB)

start 4 databases, original: adesdb (version 1: motion + ades database) and source ADES2 database from UIBK repository; target: ades2db (version2, ades database only) and motiondb

```shell
ROS_NAMESPACE=/target rosrun uibk_libades_ros ades2db --home /common/homes/staff/imagine-user/repo/ades_database_kitgripper_git/ades2_1/ades2db
ROS_NAMESPACE=/target rosrun uibk_libades_ros motiondb --home /common/homes/staff/imagine-user/repo/ades_database_kitgripper_git/ades2_1/motiondb
ROS_NAMESPACE=/sym rosrun uibk_libades_ros ades2db --home /common/homes/staff/imagine-user/imagine_ws/src/imagine_ades/uibk_ades_db/ADESDB_v2_final/ades_db
ROS_NAMESPACE=/subsym rosrun uibk_libades_ros adesdb --home ~/repo/ades_database_kitgripper
rosrun uibk_libades_ros merge_symb_subsymb_db.py
```

In the terminal, it requires user input
```
suck
['regular_suction']
Existing motion sequences for symb ADES
lever
['lever_head', 'lever_headcable', 'lever_magnet']
Existing motion sequences for symb ADES
unscrew
NO existing motion sequences for symb ADES
[]
Enter new motion sequence name (or q to exit): regular_unscrew
Enter new motion sequence score: 1
Enter new motion sequence input types: 
Enter new motion sequence motions name (separated by ' '): 
Motion per sequence
{'regular_unscrew': []}
Enter new motion sequence name (or q to exit): q
shake-away
NO existing motion sequences for symb ADES
[]
Enter new motion sequence name (or q to exit): regular_shake
Enter new motion sequence score: 1
Enter new motion sequence input types: 
Enter new motion sequence motions name (separated by ' '): 
Motion per sequence
{'regular_shake': []}
Enter new motion sequence name (or q to exit): q
flip
NO existing motion sequences for symb ADES
[]
Enter new motion sequence name (or q to exit): regular_flip
Enter new motion sequence score: 1
Enter new motion sequence input types: 
Enter new motion sequence motions name (separated by ' '): 
Motion per sequence
{'regular_flip': []}
Enter new motion sequence name (or q to exit): q
```


### Conversion from ADES2 to ADES2_1

you need to start 4 databases, 1 for symbolic ADES2DB and one for motiondb that matches ADES2DB. The last two are the target ADES2_1DB and the corresponding motiondb. Make sure `pyyaml` is installed in python2 and run the conversion script using ros + python2

```shell
ROS_NAMESPACE=/orig rosrun uibk_libades_ros ades2db --home /common/homes/staff/imagine-user/repo/ades_database_kitgripper_git/ades2/ades2db
ROS_NAMESPACE=/orig rosrun uibk_libades_ros motiondb --home /common/homes/staff/imagine-user/repo/ades_database_kitgripper_git/ades2/motiondb
ROS_NAMESPACE=/target rosrun uibk_libades_ros ades2_1db --home /common/homes/staff/imagine-user/repo/ades_database_kitgripper_git/ades2_1/ades2db
ROS_NAMESPACE=/target rosrun uibk_libades_ros motiondb --home /common/homes/staff/imagine-user/repo/ades_database_kitgripper_git/ades2_1/motiondb
rosrun uibk_libades_ros convert_database_ades2_to_ades2_1.py
```

### update ades motion sequence in ADES2_1DB

```
rosservice call /ades/ades2_1db/update_ades_motion "ades_name: 'lever'
sequence_name: 'lever_magnet'
sequence_motion_names:
- 'LeverMagnetLeft'
- 'LeverMagnetRight'
- 'RemoveMagnetFromTool'"
```

```
rosservice call /ades/ades2_1db/update_ades_motion "ades_name: 'flip'
sequence_name: 'regular_flip'
sequence_motion_names:
- 'flip'"

rosservice call /ades/ades2_1db/update_ades_motion "ades_name: 'shake-away'
sequence_name: 'regular_shake'
sequence_motion_names:
- 'shake'"

rosservice call /ades/ades2_1db/update_ades_motion "ades_name: 'unscrew'
sequence_name: 'regular_unscrew'
sequence_motion_names:
- 'unscrew'"

rosservice call /ades/ades2_1db/update_ades_motion "ades_name: 'suck'
sequence_name: 'regular_suction'
sequence_motion_names:
- 'SuckingAction'"
```


## Action and parameter format

ades name	            | action parameter	| type	| comment 
--                      |--                 |--     |--
flip                    | None              |       | None param required
shake-away	            | None              |       | None param required
unscrew	                | ScrewPose	        | 2	    |
lever	                | LeverPose	        | 2     |	
suck	                | SuckPose	        | 2	    |
cut                     | CutPose           | 2     | 
push                    | PushPose, PushDir | 2, 2  | As we discussed, KIT will fix the parameter for now, so none param required
teach                   | None              |       |  None param required
switch-tool	            | ToolIndex	        | 0	    |
OpenJawAction	        | None              |       | None param required
InfoGatheringAction	    | None              |       | None param required
reset                   | None              |       | None param required
MoveArmToPoseAction     | PoseName          | 0     | 
PickingUpAction         | None              |       | None param required
RotatingBaseAction      | FlipAngleInRadian | 1     | 
SwitchLightAction       | SwitchFlag        | 0     |  
SwitchPumpAction        | SwitchFlag, ValveReverseFlag | 0, 0 | 
