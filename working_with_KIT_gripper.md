![logo](./images/logo.png)


# Working With The KIT-Multifunctional Gripper

## Get the Database

clone the ades database to your local filesystem

```shell
cd PATH_TO_ADES_DATABASE
git clone https://github.com/wyngjf/ades_database_kitgripper
```

## Train the ScrewLocalizer

***<u>Note that, the screw localizer model in the repository is already pretrained and ready to use.</u>***

If you decide to train the model again. Here's the instruction:

- find the database here: https://github.com/wyngjf/kit_gripper_screw_database and clone it to /path_to_training_data
depend on which Demo, you need to choose the SETUP: 
    - SortedImg_KIT:  for KITDemo with KIT setup
    - SortedImg_innsbruck: for ProjectDemo with UIBK setup

train the model with the following command:

- setup an virtual environment e.g. with Anaconda. 
- install tensorflow with gpu support of version 1.15.0 and tflearn of version 0.3.2
- run the training script located in Imagine project and specify the corresponding directories that stores images of labeled screws.

```shell
conda create -n tf python=3.6
conda activate tf
pip install tensorflow-gpu==1.15.0 tflearn=0.3.2
~/path_to_imagine_project/etc/scripts/convNet.py.in --pos_input_dir=/path_to_training_data/SETUP/Screw --neg_input_dir=/path_to_training_data/SETUP/NoScrew --is_train --old_model_dir /path_to_imagine_project/data/Imagine/ScrewLocalizer/
```

if screw is not well detected, collect new training data and sort them using the script inside this repo.

## Configure ArmarX

In order to let armarx find the required ArmarX packages in Imagine project, you need to edit your local configuration files in `~/.armarx/armarx.ini` and here is an example.

```shell
~/.armarx$ cat armarx.ini  
[DEFAULT]
profile = gripper

[AutoCompletion]
armarx-dev = 1
packages = ImagineRT,VisionX,Imagine,RobotSkillTemplates
armarx = 1
```

By default, ArmarX uses a default profile configuration, the profile configures the ip-address and port of network connection for ArmarX and MongoServer. You can setup a new profile, let's say ***gripper***, by running the following. this will generate a folder in the profiles folder called gripper.

```shell
~/.armarx$ armarx switch gripper
~/.armarx/profiles$ ls
default  gripper
~/.armarx/profiles$ ls gripper 
default.cfg  default.generated.cfg  icegrid-sync-variables.icegrid.xml
```

Then you need to edit the last line of `default.cfg` to put the 3 Imagine-related package names at the last line **`ImagineRT,Imagine,RobotAPI`**.

```
Ice.Default.Locator=IceGrid/Locator:tcp -p 19620 -h localhost
IceGrid.Registry.Client.Endpoints=tcp -p 19620 -h localhost
#Ice.Default.Host=127.0.0.1

ArmarX.MongoHost=localhost
ArmarX.MongoPort=19621

#Put your custom ArmarX Packages in this list, e.g. so that the gui can find their plugins.
ArmarX.AdditionalPackages=ImagineRT,Imagine,RobotAPI
```

You are done with setting up ArmarX for Imagine project.


## Start the Software

- **start the ROS master**

    - if you want to start ROS master and ArmarX on the same machine, start local master in new terminal:  `roscore`
    - if you run them on different machines and communicate over network, use remote ros master, you need to setup your network where the rosmaster exists and export the URI and IP address in the terminal.
    
    ```
    export ROS_MASTER_URI=http://160.69.69.100:11311
    export ROS_IP=160.69.69.174
    ```
    where the ros_master_uri and the ros_ip can be found with `ifconfig`

- **start ADES database**

    in new terminal

    ```
    source ~/PATH_TO_ROS_Workspace/devel/setup.bash
    rosrun uibk_libades_ros adesdb --home /PATH_TO_ADES_DATABASE/ades_database_kitgripper'
    ```

    wait until `Starting main loop.` shows in the terminal
    
    you can use `rosservice call /adesdb/list_ades` to list the actions in ades, make sure you source the ros workspace before running service call.


- **start ArmarX and the GUI interface**

    ```
    armarx start
    armarx memory start
    armarx gui
    ```

    You can choose to open an empty GUI. And you will see the following pop-up window.

    ![armarx_gui](./images/armarx_gui_annotation.png)

    If you **don't see the icons** or **some of them are missing**, find it by searching the name in the **search panel** and click the `+` icon right next to the search panel. The widget will be added to the current GUI interface.

    We have already **pre-configured some GUI layout** stored in the Imagine project. You can load them by clicking `File → Load Gui Config. → <choose the file>` from the following path `Imagine/data/Imagine/GuiConfig/`. You will see `ImagineDev.armarxgui` and  `ImagineDemoVisualization.armarxgui`. 
    
    You can also **quickly access recently used GUI config** from `File  → Recently Opened Files` and **store** your GUI configurations by `File  → Save Gui Config. as`.

## Setup Scenario

***<u>Note that you only need to do this once. The GUI will remember your opened scenario and your configurations next time</u>***

***<font color="red">However, if you need to change the Scenario Properties or need to use new configuration file, you need to set it up accordingly and restart the scenario to see the effect.</font>***

### Open Scenario

- click the `ScenarioManager` or search it in the search panel and add the widget.
- Open the existing Scenarios as shown in the figure

![scenario](./images/armarx_scenario_annotation.png)

- In the OpenScenarioView window, search `KITGripper` and click open. 
- Do the same thing to open `KITGripperHighLevel` scenario
- ***<u>Note that you only need to do this once. The GUI will remember your opened scenario next time</u>***


#### Add new scenarios in the Edit mode

By clicking `Edit Mode` on the top of the Scenario Manager Tab, you will see a side bar **Application Database** containing all available applications/components under your current setup. Type in the **search bar**, e.g. `screw`, you will see the `ScrewLocalizerApp`. Simply **Drag-And-Drop** it from the sidebar to the **Top level scenarios**, e.g. `KITGripperHighLevel`. It will automatically place it in the correct category, here the `KITGripperHighLevel/Imagine`, where `Imagine` indicating the package name where the source of `ScrewLocalizerApp` is located.

![add-scenario](./images/add_scenario.png)


### Configure you network connection:

![startGripper](./images/armarx_gui_start.png)

In Scenario `KITGripper`, expand the entry by double clicking `KITGripper →  ArmarXCore → startGripper`, find property `ApplicationArguments`: use your network device name (e.g. `enpXXX`).

### Configure the robot hardware information

![caution](./images/caution.jpeg)

In Scenario `KITGripper`, find `KITGripper → ImagineRT → ImagineUnitApp` and set `BusConfigFilePath` to the corresponding robot hardware configuration file, which can be found in `ImagineRT/data/ImagineRT/HardwareConfig/`

We have 3 hardware configuration files for each of the Gripper V3. You can find the tag next to the Gripper Suction Cup, saying e.g. `G3.1`, `G3.2` or `G3.3`.

E.g. set the property value to `ImagineRT/HardwareConfig/KITGripper_g3-1.xml` for `G3.1` and so on.

In addition you have to set the property `ArmarX.GripperSafety.GripperNumber` in the  `KITGripper → ImagineRT → GripperSafety` to either `3.1`, `3.2` or `3.3`. This property will be used by other components to determine which gripper is currently used.


### Configure WebCam connection

In Scenario `KITGripper`, find `KITGripper → VisionX → WebCamImageProviderApp` and set the property `DeviceNumber` to your USB device number that connected to the Gripper onboard camera.

you can find the device number by
```shell
ls /dev | grep video
```

## Power-on the robot

Connect the power supply and the emergency stop correctly and pull the emergency stop button to power on the robot. You will hear the sound of the cooling fans. 

## Start the Scenario

![caution](./images/caution.jpeg)

***<font color="red">
Note that the robot need a initilization process, i.e. to calibrate the gripper jaw joint, since there's no absolute encoder.</font>***

***<font color="red">When you start the `startGripper` scenario, the gripper jaw joint will close automatically until the joint exceed the torque limit. This means that you should make sure there is nothing in between of the two gripper panels. Otherwise the gripper jaw joint is not calibrated and might cause damage of this joint.
</font>***

***<font color="red">If unfortunately the HDD is tightly grasped by the jaw joint, you can still start the  `startGripper` scenario with a wrong calibration of the jaw joint. And open a `KinematicUnitGui` widget and set the `KIT_Gripper_Jaw` joint to `velocity` controlled mode, and the drag the slide bar to the left, this will set target velocity to the jaw joint to open a little bit, stop until you can freely move out the HDD. <u>Don't forget to restart the `startGripper` to re-calibrate the jaw joint.</u>
</font>***

- make sure nothing is grasped by the jaw joint. 
- start the `KITGripper` scenario by clicking the triangle start icon. You can also start the sub-scenarios one by one.
- minimum requirement of HighLevel Scenarios, start the following scenario
    - `KITGripperHighLevel/RobotAPI`, 
    - `KITGripperHighLevel/Imagine`, 
    - `KITGripperHighLevel/ArmarXGui`
- To give a demo with ROS connected, you should start the whole `KITGripperHighLevel` scenario

## Use KinematicUnitGui to control the robot

You can control a single joint of the robot via this GUI interface.

![TODO](add_kin_gui_figure)

## Run Statechart from ArmarXGui

- open a Statechart Widget and choose `ImagineReal` in the pop up window, click OK 

    ![statechart](./images/statechart_start.png)

- you will see all the statechart groups on the left side

    an example of SwitchToolAction looks like this. You can run the statechart by clicking the start triangle icon below, it will change to a stop cross icon once it starts. You can stop the statechart by clicking the cross icon.

    ![statechart_eg](./images/statechart_example.png)

- Note that: if the statechart doesn't start and you see the information like "waiting for dependencies: xxxx", this means that the applications/components in the scenario somehow died or you forget to start them, you will see 'mixed' information in the corrosponding scenario entry in the `ScenarioManager` widget, you need to restart them.

    ![wait](./images/wait_depends.png)

## Run Statechart Actions from ROS

To run the statechart from ROS, you have to start the following two scenarios: `KITGripperHighLevel/Imagine/ROSTaskProverBrdgeApp` and the whole `KITGripperHighLevel/ArmarXCore`.

Examples of how to run statechart actions from ROS are given in the `ros2armarx` package in this file 
`ros2armarx/scripts/test_asc2armarx_bridge.py`

```
git clone git@github.com:IMAGINE-H2020/imagine_ros2aramrx.git
```

Run the program

```
python test_asc2armarx_bridge.py
```
you will see all the options below:
```
actions to choose, enter a number in the range [0 - 17]:
0       SuckingAction
1       TeachInAction
2       UnscrewingAction
3       MoveArmToPoseAction
4       SwitchToolAction
5       RotatingBaseAction
6       FlippingAction
7       ShakingAction
8       InitializeRobotAction
9       PickingUpAction
10      CloseJawAction
11      OpenJawAction
12      SwitchPumpAction
13      ReversePumpAction
14      StopPumpAction
15      SwitchLightAction
16      TurnOffLightAction
17      PushingAction
leave it empty to exit
```
choose an action to execute and simply leave it empty and press `Enter` to exit the program.

## Visualize the Image Processing and the Robot Model during demo

- load the preconfigured GUI layout `Imagine/data/Imagine/GuiConfig/ImagineDemoVisualization.armarxgui`
- make sure the following scenarios are started:
    - `KITGripper`
    - `KITGripperHighLevel/Imagine/GripperSafety`
    - `KITGripperHighLevel/Imagine/BitLocalizerApp`
    - `KITGripperHighLevel/Imagine/ScrewLocalizerApp`
    - `KITGripperHighLevel/Imagine/LeverOrientationDetectionApp`

## Stop The Robot:

- stop all the scenarios by clicking the cross icon
- stop armarx by `Ctrl+C` in the terminal where you started `armarx gui`.

    if it's not stopped, you can run `armarx killAll` in another terminal. Then

    ```shell
    armarx stop
    armarx memory stop
    ```

    sometimes you need `pkill -9 ArmarXGuiRun` to kill the GUI process.

- `Ctrl + C` (only once) in the terminal in which you start ADES database, <font color="red">wait until the process is totally terminated, even the following shows up in terminal, the process is still writing the data to files, so wait until you can run another command in this terminal</font>, the following will show up:

    ```
    Spinning finished, exiting …
    Done.
    stop other process
    ```

- poweroff the robot by pushing down the emergency button.
