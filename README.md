![logo](./images/logo.png)

# KIT Gripper

![gripper](./images/title_image.png)

[This Page](https://h2t.gitlab.io/imagine-documentation): https://h2t.gitlab.io/imagine-documentation

[Project Official Website](https://imagine-h2020.eu/): https://imagine-h2020.eu/

## Demonstration:
YouTube Videos



1. The demonstration of disassembly actions with predefined action sequences and parameters, as well as the robot-assisted kinesthetic teaching for transfering skills from human to the robot

    <iframe width="560" height="315" src="https://www.youtube.com/embed/VNjY7aSKcwc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

2. The demonstration of the whole IMAGINE system, including ADES, state-estimation, ASC, planner, PHYS simulation. 

    <iframe width="560" height="315" src="https://www.youtube.com/embed/5l-eJ-dDqFk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

