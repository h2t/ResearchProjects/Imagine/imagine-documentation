![logo](./images/logo.png)

# Setup ROS Environment

## Install ROS melodic

install ros-melodic, create a workspace according to the [Official Instructions](http://wiki.ros.org/melodic)

## Required Packages

**install dependencies:**

```shell
sudo apt install libarmadillo-dev
sudo apt install libopenblas-dev
sudo apt install liblapack-dev
sudo apt install libmlpack-dev
```

---

**install libgp:**

```shell
git clone https://github.com/mblum/libgp.git
cd libgp
mkdir build
cd build

# root install
cmake .. && make && sudo make install

# local install, e.g. at ~/local/
xargs rm < install_manifest.txt
cmake -DCMAKE_INSTALL_PREFIX=$HOME/local/ -DBUILD_TESTS=OFF .. && make -j12 && make install
```
---

**caros packages**

compile the following 3 packages from [caros](ttps://gitlab.com/caro-sdu/caros) in your ros workspace:

```shell
git clone git@gitlab.com:caro-sdu/caros.git
```

you only need to copy these 3 pkgs to your ros workspace and build them

- `caros_common` and `caros_common_msgs` in `core` folder
- `caros_control_msgs` in `interface` folder


**Install SOFA**

Download CMake 3.18 and extract it into the folder `~/local/packages/cmake-3.19.8`. Setup an conda Environment with python 3.7. And install pybind11 with conda, (not pip, otherwise, cmake files are missing)

create a symlink to the python executable in `./local/bin` 

```shell
cd ~/.local/bin
ln -s ~/anaconda3/envs/tf2/bin/python3 python3
```

And

```shell
tf2
conda install pybind11
mkdir -p ~/repo/sofa 
cd ~/repo/sofa
git clone https://github.com/sofa-framework/sofa.git src
git checkout ec27e23b4e81b297dbb63a40e1954b410ab7745f
cd src/application/plugin/SofaPython3 
git checkout dc2ac53e613b9f6f07559d4508191f24895d2b1e
mkdir build
cd build
~/local/packages/cmake-3.19.8/bin/cmake ../src  -DPLUGIN_SOFAPYTHON3=ON -DSOFA_BUILD_TEST=OFF
make -j16
```

---

**Install Imagine packages:**


1. imagine_ades/uibk_ades: https://github.com/IMAGINE-H2020/imagine_ades (cmake)

    dependencies:
    - mlpack [http://www.mlpack.org/]
    - libgp [https://github.com/mblum/libgp]
    - boost serialization [http://www.boost.org/]

    ```shell
    git clone git@github.com:IMAGINE-H2020/imagine_ades.git
    cd imagine_ades/uibk_ades
    mkdir build && cd build
    cmake -DLibGP_INCLUDE_DIR=$HOME/local/include -DLibGP_LIBRARIES=$HOME/local/lib -DCMAKE_INSTALL_PREFIX=$HOME/local/ .. && make -j12
    make install
    ```
2. vision_msgs: https://github.com/Kukanani/vision_msgs (ros workspace)
2. imagine-common: https://github.com/IMAGINE-H2020/imagine_common (ros workspace) 
3. imagine_ades/libades_ros: https://github.com/IMAGINE-H2020/uibk_libades_ros (ros workspace) 

    change the cmake to make it work with the local installation of it's dependencies

    ```
    # set the CMAKE paths as
    set(CMAKE_MODULE_PATH "/usr/lib/cmake" "~/local/lib/cmake")
    set(CMAKE_PREFIX_PATH "/usr/local/lib/cmake/libades" "~/local/lib/cmake/ades")
    ```
    Add libgp paths for lib and include
    ```
    # include
    find_path(LibGP_INCLUDE_DIR
    NAMES gp.h
    PATHS /usr/local/include/gp/ /common/homes/staff/imagine-demo/local/include/gp/
    )
    # lib
    find_library(LibGP_LIBRARIES
    NAMES libgp.a
    PATHS /usr/local/lib/ /common/homes/staff/imagine-demo/local/lib/
    )
    ```
4. Planner:
    # https://github.com/IMAGINE-H2020/imagine_planning/tree/master/csic_imagine_planner
    
    ```shell
    mkdir build
    cd build
    cmake -DADES_DIR=$HOME/local/lib/cmake/ades -DLibGP_INCLUDE_DIR=$HOME/local/include -DLibGP_LIBRARIES=$HOME/local/lib -DCMAKE_INSTALL_PREFIX=$HOME/local/ .. && make -j10
    make install
    ```
5. ROS workspace

    in ros ws
    
    ```shell
    catkin build -DLibGP_INCLUDE_DIR=$HOME/local/include/ -DLibGP_LIBRARIES=$HOME/local/lib -DADES_DIR=$HOME/local/lib/cmake/ades
    ```

## Setup a separate workspace for cv-bridge

- follow the following page https://cyaninfinite.com/ros-cv-bridge-with-python-3/
- add python path to `.bashrc` so that Python will first look into the path where we compiled `cv_bridge`
    
    ```
    export PYTHONPATH=/common/homes/staff/imagine-user/cvbridge_build_ws/build/cv_bridge/lib:$PYTHONPATH
    ```

## Configure QtCreator

configure your qtcreator to find the ros packages

```
source ~/PATH_TO_ROS_Workspace/devel/setup.bash
```

start qtcreator from the same terminal or add this to your .bashrc
